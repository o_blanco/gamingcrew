<?php
class Games {

    public function run() {
        return <<<HTML

        <section id="left"xmlns="http://www.w3.org/1999/html">

            <article class="gameWrapper">
            <header><img src="img/arrow.png"/> &nbsp;&nbsp; The Esfalerante</header>
            <div>
                <!-- Scirra Arcade Embed for 'The Esfarelante'.  Please see 'scirra.com/terms-and-conditions' -->
                <iframe src="//static1.scirra.net/arcade/games/5471/play" width="740" height="416" style="width:740px;height:416px;border:0;margin:0;padding:0;" frameborder="0" id="ScirraGame5471" title="Make HTML5 games with Construct 2"></iframe>

                <div class="info">
                    <table>
                    <thead>Instrucciones</thead>
                    <tbody>
                        <tr>
                            <td>Movimiento</td><td>Flechas izquierda y derecha / Teclas A y D </td>
                        </tr>
                        <tr>
                            <td>Saltar</td><td>Flecha arriba / W </td>
                        </tr>
                        <tr>
                            <td>Morder</td><td>Ctrl / teclas O o P </td>
                        </tr>
                        <tr>
                            <td>Pausa</td><td>ESC </td>
                        </tr>
                        <tr>
                            <td>Especial</td><td>Tecla L </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            </article>

            <article class="gameWrapper">
            <header><img src="img/arrow.png"/> &nbsp;&nbsp; Super Ubi Land</header>
            <!-- Scirra Arcade Embed for 'Super Ubi Land'.  Please see 'scirra.com/terms-and-conditions' -->
            <div>
                <iframe src="//static1.scirra.net/arcade/games/1487/play" width="800" height="450" style="width:800px;height:450px;border:0;margin:0;padding:0;" frameborder="0" id="ScirraGame1487" title="Make HTML5 games with Construct 2"></iframe>
                <div class="info">
                    <table>
                    <thead>Instrucciones</thead>
                    <tbody>
                        <tr>
                            <td>Movimiento</td><td>Flechas izquierda y derecha / Teclas A y D </td>
                        </tr>
                        <tr>
                            <td>Saltar</td><td>Flecha arriba / Otra vez en el aire para doble salto </td>
                        </tr>
                        <tr>
                            <td>Planear</td><td>Flecha arriba mientras estas en el aire </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            </article>

            <article class="gameWrapper">
            <header><img src="img/arrow.png"/> &nbsp;&nbsp; Sawbucks</header>
            <!-- Scirra Arcade Embed for 'Super Ubi Land'.  Please see 'scirra.com/terms-and-conditions' -->
            <div>
                <!-- Scirra Arcade Embed for 'Sawbucks'.  Please see 'scirra.com/terms-and-conditions' -->
                <iframe src="//static1.scirra.net/arcade/games/1527/play" width="640" height="640" style="width:640px;height:640px;border:0;margin:0;padding:0;" frameborder="0" id="ScirraGame1527" title="Make HTML5 games with Construct 2"></iframe>
                <div class="info">
                    <table>
                    <thead>Instrucciones</thead>
                    <tbody>
                        <tr>
                            <td>Movimiento</td><td>Flechas </td>
                        </tr>
                        <tr>
                            <td></td><td>Deja que te pille una cuchilla :D </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            </article>

            <article class="gameWrapper">
            <header><img src="img/arrow.png"/> &nbsp;&nbsp; Airscape</header>
            <!-- Scirra Arcade Embed for 'Super Ubi Land'.  Please see 'scirra.com/terms-and-conditions' -->
            <div>
                <!-- Scirra Arcade Embed for 'AirScape'.  Please see 'scirra.com/terms-and-conditions' -->
                <iframe src="//static1.scirra.net/arcade/games/848/play" width="800" height="600" style="width:800px;height:600px;border:0;margin:0;padding:0;" frameborder="0" id="ScirraGame848" title="Make HTML5 games with Construct 2"></iframe>
                <div class="info">
                    <table>
                    <thead>Instrucciones</thead>
                    <tbody>
                        <tr>
                            <td>Movimiento</td><td>Flechas derecha e izquierda</td>
                        </tr>
                        <tr>
                            <td>Saltar</td><td>Espacio</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            </article>

            <article class="gameWrapper">
            <header><img src="img/arrow.png"/> &nbsp;&nbsp; Coffe time</header>
            <!-- Scirra Arcade Embed for 'Super Ubi Land'.  Please see 'scirra.com/terms-and-conditions' -->
            <div>
                <!-- Scirra Arcade Embed for 'coffee time'.  Please see 'scirra.com/terms-and-conditions' -->
                <iframe src="//static1.scirra.net/arcade/games/3437/play" width="640" height="480" style="width:640px;height:480px;border:0;margin:0;padding:0;" frameborder="0" id="ScirraGame3437" title="Make HTML5 games with Construct 2"></iframe>
                <div class="info">
                    <table>
                    <thead>Instrucciones</thead>
                    <tbody>
                        <tr>
                            <td>Movimiento</td><td>Flechas derecha e izquierda</td>
                        </tr>
                        <tr>
                            <td>Saltar</td><td>Espacio</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            </article>

        </section>

HTML;
    }
}