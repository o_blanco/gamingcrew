<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 11/23/13
 * Time: 1:56 PM
 */

class MackingOff {
    public function run() {
        return <<<HTML

<section id="left">
    <article>
        <header>¿Como he perpetrado esta aberración?</header>
        <p>
        Este website se ha hecho utilizando PHP, HTML5, CSS3 y JavaScript (principalmente un framework llamado JQuery).
        Para la estructura básica he usado <a href="http://html5boilerplate.com/" alt="html5Boilerplate" target="_blank">HTML5Boilerplate</a>,
        una plantilla básica sin dise&ntilde;o alguno y el IDE PhpStorm.
        <br/>
        A partir de ahi, hay un script básico de entrada (index.php) que recibe las peticiones de página mediante la
        variable GET, carga la clase y ficheros relacionados (css y javascript) y ejecuta un método que devuelve
        el contenido a mostrat.
        <br/>
        Usando CSS3 he aplicado los estilos a todos los elementos y con JQuery los cambios dinámicos en la página.
        <br/>
        Para la inserción de videos y juegos he utilizado el tag iframe, que permite incrustar elementos de otras webs.
        <br/>
        Los juegos estan hechos en HTML5, no por mi obviamente, sacados de la web <a href="http://www.scirra.com" target="_blank">Scirra</a>
        que también tiene un apartado con tutoriales.
        <br/>
        El repositorio de código, en Bitbucket <a href="https://bitbucket.org/o_blanco/gamingcrew" alt="repo_gamingcrew">GamingCrew</a>.
        </p>
    </article>
    <img src="img/peleaaaa.jpg" style="height: 200px;">
</section>
HTML;
    }
} 