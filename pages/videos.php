<?php
class Videos {

    public function run() {
        return <<<HTML
    <section id="left">
    <article class="entry">
        <header>De avión en avión.</header>
        <div class="videoWrapper">
            <iframe class="youvid" width="560" height="315" src="http://www.youtube.com/embed/LrOIgxQ--Tc" frameborder="0" allowfullscreen></iframe>
        </div>
        <p class="description">
            Algo cuotidiano y que cualquier piloto de combate en la vida real sabe hacer es cambiar de avión cuando
            haga falta (me he quedado sin queroseno, se me ha metido una aveja en el habitaculo...). He aquí una
            demostración aplicada en el Battlefield 3.
        </p>
    </article>

    <article class="entry">
        <header>Mythbuters + GTAV.</header>
        <div class="videoWrapper">
            <iframe class="youvid" width="560" height="315" src="http://www.youtube.com/embed/NvEp11B83wM" frameborder="0" allowfullscreen></iframe>
        </div>
        <p class="description">
            Los Cazadores de Mitos haciendo de las suyas también en los videojuegos. Hay más episodios, buscad...
        </p>
    </article>

    </section>
HTML;
    }
}
