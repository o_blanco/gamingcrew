$(document).ready(function() {
    $('.link').hover(
        function() {
            $(this).toggleClass('shadow_text');
        }
    );

    $(window).scroll(function(){
        if ($(this).scrollTop() > 150) {
            $('#up').fadeIn();
        } else {
            $('#up').fadeOut();
        }
    });

    $('#up').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});

