/**
 * Created by oscar on 11/23/13.
 */
var rotate_step = 0;
$(document).ready(function() {
    $('.gameWrapper').click(function(){
        $('div:not(.info)', this).slideToggle('slow');
        if(rotate_step == 0) {
            deg = 180;
            rotate_step = 1;
        } else if(rotate_step == 1) {
            deg = 0;
            rotate_step = 0;
        }
        $('img', this).css({ 'transform': 'rotate(' + deg + 'deg)'});
    });

});
