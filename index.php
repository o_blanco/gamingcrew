<?php
    include_once('config/Config.php');
?>

<!DOCTYPE html>
<html class="no-js">
<head>
    <!--[if lt IE 7]>
        <html class="no-js lt-ie9 lt-ie8 lt-ie7">
    <![endif]-->
    <!--[if IE 7]>
        <html class="no-js lt-ie9 lt-ie8">
    <![endif]-->
    <!--[if IE 8]>
        <html class="no-js lt-ie9">
    <![endif]--><!--[if gt IE 8]><!--><!--<![endif]-->
    <meta charset="utf-8">

    <title>GamingGrew</title>
    <meta name="description" content="Website chorra">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/side_bar.css">
    <?php
    if(file_exists('css/'. $_GET['destination'].'.css') && $_GET['destination'] != 'home') {
        ?><link rel="stylesheet" href="css/<?php echo $_GET['destination'].'.css'?>"><?php
    }
    ?>
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <section id="page">
        <header id="header">
            <img id="logo" src="img/logo.png" alt="logo" />

            <nav>
                <a id="home_link" class="link black_back" href="index.php?destination=home">Home</a>
                <a id="games_link" class="link black_back" href="index.php?destination=games">Juegos</a>
                <a id="videos_link" class="link black_back" href="index.php?destination=videos">Videos</a>
            </nav>
            <img id="banner" src="img/banner.png" alt="banner" />
        </header>
        <section id="content">
            <?php
            if(!isset($_GET['destination'])){
                $_GET['destination'] = 'home';
            }


            // Get de next page to load
            $petition = $_GET['destination'];
            // include the class required
            if (file_exists('pages/'.$petition.'.php')) {
                include('pages/'.$petition.'.php');
                // Create a new instance of the object
                $class = new $petition;
                // The run method return the result of the action.
                // Jus show it :D
                $loadedPage = $class->run();
                // Showing :D :D

                if(!is_null($loadedPage)) echo($loadedPage);
            } else {
                include('pages/404.html');
            }

            ?>
            <section id="right">
                <?php include_once( Config::getPath().'/pages/side_bar.html') ?>
            </section>
        </section>
        <img src="img/up.png" id="up" alt="arriba" style="display: none">
        <footer>
            <section>
                <article><img src="img/oblanco2.png" alt="oblanco"/></article>
                <article>
                    <a class="link" href="index.php?destination=mackingoff" >MackingOFF</a>
                </article>
            </section>
        </footer>
    </section>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/side_bar.js"></script>
    <?php
        if(file_exists('js/' . $_GET['destination'].'.js')) {
            ?><script src="js/<?php echo $_GET['destination'].'.js'?>"></script><?php
        }
    ?>
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    <script>
        $(document).ready(function() {
            $link = '#<?php echo $_GET['destination'];?>_link'

            $('.link').each(function() {
                if($(this).hasClass('selected')) {
                    $(this).removeClass('selected')
                }

                $($link).addClass('selected');
            });
        })
    </script>
</body>
</html>